// 00
package main

import (
	"fmt"
	"math/rand"
	"os"
	"strings"

	//"fmt"
	"io/fs"
	//"path/filepath"
	//"path/filepath"
)

func WriteFileS(sFileName string, sText string) {

	if len(sFileName) == 0 {
		fmt.Println("Error! fileName empty!")
		os.Exit(1)
	}

	if len(sText) == 0 {
		return
	}

	file, err := os.Create(sFileName)

	if err != nil {
		fmt.Println("Unable to create file:", err)
		os.Exit(1)
	}
	defer file.Close()
	file.WriteString(sText)
}

func WriteFileAppendS(sFileName string, sText string) {

	file, err := os.OpenFile(sFileName, os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		fmt.Println("Unable to create file:", err)
		os.Exit(1)
	}
	defer file.Close()
	file.WriteString(sText)
}

func WriteFileBin(sFileName string, bBin []byte) {

	file, err := os.Create(sFileName)
	if err != nil {
		fmt.Println("Unable to create file:", err)
		os.Exit(1)
	}

	defer file.Close()

	file.Write(bBin)

}

// Получаем массив файлов в директории dirPath
func arrFiles(dirPath string) []string {

	// Открываем директорию
	dir, err := os.Open(dirPath)
	if err != nil {
		fmt.Println("Error! Open(dirPath) - Ошибка открытия директории:", err, dirPath)
		return nil
	}
	defer dir.Close()

	// Создаем список файлов и директорий
	filesArr, err := os.ReadDir(dirPath)
	if err != nil {
		fmt.Println("Ошибка чтения директории:", err)
		return nil
	}

	arrf := arrF(&filesArr, dirPath) // Получаем слайс имен файлов в директории dirPath
	return arrf
}

// Получаем массив имен файлов в директории dirPath
func arrF(filesArr *[]fs.DirEntry, dirPath string) []string {
	max := len(*filesArr)       // число файлов в директории
	arrf := make([]string, max) // массив имен файлов

	// список файлов ".mp3"
	fName := ""
	j := 0
	for i, file := range *filesArr {
		fName = strings.ToLower(file.Name())
		if strings.HasSuffix(fName, ".mp3") {
			fmt.Println("+ ", i, fName)
			arrf[j] = dirPath + string(os.PathSeparator) + fName
			j++
		} else {
			fmt.Println("- ", i, fName)
		}
		//fmt.Println(file.Name())
	}

	sl := arrf[:j]
	fmt.Println(len(sl), "/", len(*filesArr))
	return sl
}

// Записать в файл m_DDHH.m3u строки из массива sl
// DD : [0..30]
// HH : [8-21]

func makeLists(sl *[]string, iLen int) {

	iMaxVal := len(*sl)
	indexs := make([]int, iLen)
	sDD := ""
	sHH := ""
	sTxt := ""

	for day := 0; day < 32; day++ {
		sDD = fmt.Sprintf("%02d", day+1)

		for hour := 8; hour < 22; hour++ {
			sHH = fmt.Sprintf("%02d", hour)

			sFileName := "m_" + sDD + sHH + ".m3u"

			// заполнить массив случайных индексов
			rndArrIndex(&indexs, iMaxVal)

			// получить текст из массива sl
			sTxt = textM3u(sl, &indexs)

			WriteFileS(sFileName, sTxt)

			fmt.Println("\nsFileName	:", sFileName)
			fmt.Println("sTxt	:", sTxt)
		}
	}

	/*
		UnixMicro := time.Now().UnixMicro()
		fmt.Println("UnixMicro	:", UnixMicro)

		UnixNano := time.Now().UnixNano()
		fmt.Println("UnixNano	: ", UnixNano)

		imax := len(*arrF)

		iR := Rnd(imax)
		fmt.Println("iR			: ", iR)
	*/
}

// Получить текст (случайные строки) из массива строк sl
// (*indexs)
func textM3u(sl *[]string, indexs *[]int) string {
	pSl := *sl
	pIdxs := *indexs

	// s:=(*indexs)[0] // - Ok

	sText := ""
	indx := 0
	for i := 0; i < len(pIdxs); i++ {
		indx = pIdxs[i]
		sText = sText + pSl[indx] + "\n"
	}

	return sText
}

func Rnd(iMax int) int {
	iR := rand.Intn(iMax)
	return iR
}

// Получить массив случайных индексов 0...iMax
func rndArrIndex(indexs *[]int, iMax int) int {

	ptrIndxs := *indexs
	iVal := 0
	for i := 0; i < len(*indexs); i++ {
		iVal = rand.Intn(iMax)

		ptrIndxs[i] = iVal
		//*(indexs[i]) = iVal
	}

	fmt.Println("indexs : ", indexs)

	return 0
}

func main() {

	dir, _ := os.Getwd()
	if len(os.Args) < 2 {
		// ok
	} else {
		dir = os.Args[1]
	}

	// получить массив файлов mp3 в директории dir
	arrF := arrFiles(dir)

	// создать файлы m3u. По 3 случайных строки .mp3 в каждом файле
	makeLists(&arrF, 10)

	/*
		fmt.Println("arrF			: ", arrF)
		fmt.Println("s 				: ", s)
		sep = s[0:10]
		fmt.Println("sep  s[0:10] 	: ", sep)

		sData := "Hello All!"
		WriteFileS("test1.txt", sData)

		WriteFileAppendS("test1.txt", "\nHello Again!")

		bData := []byte("Hello Bold!")
		WriteFileBin("test2.txt", bData)

		fmt.Println("Done.")
	*/

}
